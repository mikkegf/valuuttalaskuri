<?php
require_once 'class/Currency.php';
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Valuuttalaskuri</title>
</head>
<body>
    <h3>Valuuttalaskuri</h3>
<?php
$result = $currencyObject = new Currency();
if ($_SERVER['REQUEST_METHOD']=== 'POST') {
    $money = filter_input(INPUT_POST,'amount',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $currencyObject->settoCurrency('GBP');
    $result = $currencyObject->calculate($money);
}
?>
    <form action=<?php print $_SERVER['PHP_SELF']; ?> method="post">
        <div>
            <label>EUR</label>
            <input type="number" name="amount" step="0.01">
        </div>
        <div>
            <label>GPB</label>
            <output><?php printf("%.2f", $result); ?></output>
        </div>
        <button>Laske</button>
    </form>
</body>
</html>