<?php
const API_URL  ='https://api.exchangeratesapi.io/latest';
class Currency {
    private $to = '';
    
    

    public function setToCurrency($value) {
        $this->to = $value;
    }

    public function calculate($amount) {
        $conversion = $amount * 0.89;
        return $conversion;
    }

}
?>